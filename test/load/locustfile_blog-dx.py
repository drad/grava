from locust import HttpLocust, TaskSet, task
# install: `pip install locustio`
# start server: `locust -f locustfile_blog-dx.py`
# run/monitor test: http://localhost:8089/

test_version = "2.0.0"
about = {
    "name": "locustfile_blog-dx.py",
    "version": "2.0.0",
    "modified": "2020-03-04",
    "created": "2019-05-19",
}

class WebsiteTasksBasic(TaskSet):

    @task(5)
    def index(self):
        self.client.get("/")

    @task(3)
    def kailie(self):
        self.client.get("/kailie")

    @task(1)
    def natalie(self):
        self.client.get("/natalie")

    @task(1)
    def addalie(self):
        self.client.get("/addalie")

    @task(2)
    def family(self):
        self.client.get("/family")

    @task(1)
    def family_casa_del_lago(self):
        self.client.get("/family/nh-cabin")

    @task(1)
    def family_casa_del_lago_2019_christmas_break(self):
        self.client.get("/family/nh-cabin/2019-christmas-break")

    @task(1)
    def family_casa_del_lago_bottom_deck(self):
        self.client.get("/family/nh-cabin/bottom-deck")

    @task(3)
    def recipes(self):
        self.client.get("/recipes")

    @task(3)
    def recipes_breakfast(self):
        self.client.get("/recipes/breakfast")

    @task(3)
    def recipes_breakfast_biscuits_from_scratch(self):
        self.client.get("/recipes/breakfast/biscuits-from-scratch")

    @task(3)
    def recipes_bread_dr_whole_wheat_2(self):
        self.client.get("/recipes/bread/dr-whole-wheat-2")



class WebsiteUser(HttpLocust):
    task_set = WebsiteTasksBasic
    # ~ host = "http://localhost:3000"
    host = "https://blog.dradux.com"
    # ~ host = "http://uktest.wwnorton.com"

    min_wait = 5 * 1000   #5 seconds
    max_wait = 15 * 1000   #15 seconds
