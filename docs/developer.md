# Developer


### Monthly Update Process

- update app `Dockerfile` and `_version.json`
- update web `Dockerfile` and `_version.json`
- update `version` and `appVersion` in `Chart.yaml`
- update redis, web, and varnish `image` in `values.yaml.prod`
- locally build app image at least as PHP updates usually have issues
- review, commit, tag and push code change
- build and push images
- deploy
