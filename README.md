# Docker Image for Grav

GRAVA is a docker-based (alpine) "wrapper" instance of [GRAV](https://getgrav.org/). [GRAV](https://getgrav.org/) is not part of this application itself, rather this application provides the web and app server logic for [GRAV](https://getgrav.org/).

## Features ##

- web / app servers are abstracted and can be scaled independently
- small memory footprint thanks to lighttpd (as little as 200mb for app and 200mb for web)
- small disk footprint thanks to alpine (95.3m for app and 14.9m for web)
- GRAV data stored externally (Volume) to allow scaling
- Caching - OPcache & APCu bundled in app container


## About ##

The goal of this project is to provide a simple and lightweight wrapper for [GRAV](https://getgrav.org/) which can be ran in docker/k8s environments.

Components:

- Web Server: lighttpd
- Application Server: PHP

As this is a wrapper around [GRAV](https://getgrav.org/), it is rather basic. It also assumes you have downloaded and set up [GRAV](https://getgrav.org/) or otherwise have it setup external to this project.

As stated above, this app has been designed to run in k8s. The docker-compose.yml file provides the basic structure/information for the app and can be used to run the app locally (for testing) or in a 'production' scenario if you choose.


## Notices ##

- this is a "wrapper" project for a [GRAV](https://getgrav.org/) instance - you need to download/extract a version of [GRAV](https://getgrav.org/) - see the 'Local Setup Example' section for more info.


## Local Setup Example ##

If using as a local (docker-compose based) instance or via k8s you will need to download/extract [GRAV](https://getgrav.org/) somewhere and point to it.

If you are using docker-compose and the docker-compose.yml file you can set it all up as follows:
- copy .env.template to .env
  - adjust the .env file as needed; however, it should not require changes to get started
- create a tmp directory in the project root
- download [GRAV](https://getgrav.org/) into the tmp directory
- extract the [GRAV](https://getgrav.org/) distribution

If you downloaded grav+admin this will leave you with {PROJECT_ROOT}/tmp/grav-admin/{all-the-grav-files}. The docker-compose.yml file is already set for this patch structure so you can simply `docker-compose up -d` and everything should be started up.

Point browser to `http://localhost/8007` and create user account...


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- ensure the `dx-blog-data` nfs share has been set up (pv and pvc will be created but the share itself needs to be created)
- deploy the chart: `helm install dx-blog dx-blog/ --values dx-blog/values.yaml.prod --create-namespace --namespace dradux-blog --disable-openapi-validation`
  - list chart: `helm list -n dradux-blog`
  - update: `helm upgrade dx-blog dx-blog/ --values dx-blog/values.yaml.prod --namespace dradux-blog --disable-openapi-validation`
  - rollback: `helm rollback -n dradux-blog dx-blog`
  - uninstall: `helm uninstall -n dradux-blog dx-blog`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n dradux-blog deployment app web varnish`
    + `kubectl rollout restart -n dradux-blog statefulset redis`

