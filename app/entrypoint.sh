#!/bin/sh
# NOTICE: grav (the app) has things hard coded to /var/www/html so I suggest you leave
#         the following alone unless you know what you are doing.

app_root="/var/www/localhost/htdocs"
extra_php_confs_dir="/mnt/extra-php-confs"
php_confs_dir="/usr/local/etc/php/conf.d"
_name="entrypoint.sh"
_version="1.4.1"

echo "${_name} v${_version}"
echo "- app_root:            ${app_root}"
echo "- php_confs_dir:       ${php_confs_dir}"
echo "- extra_php_confs_dir: ${extra_php_confs_dir}"
echo "- -------------------- ---------------------------------"

chmod a+w /dev/stderr
chmod a+w /dev/stdout

# mount a volume to /mnt/extra-php-confs and its contents will be added to php_confs_dir.
echo "checking for extra-php-confs..."
if [ -d "${extra_php_confs_dir}" ] ; then
    echo " - extra php confs found, hotloading..."
    cp -rafL ${extra_php_confs_dir}/*.ini ${php_confs_dir}/
    echo " - php_confs_dir is now:"
    ls -lah "${php_confs_dir}"
else
    echo " - no extra php confs found..."
fi

# start php-fpm
echo "starting php-fpm: $(date +"%F %H:%M:%S")"

# start php-fpm
php-fpm --nodaemonize -F

# start php-fpm daemonized so we can run something after
#php-fpm -D

#echo "just keep container running..."
#tail -f /dev/null
