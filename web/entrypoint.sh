#!/bin/sh
# NOTICE: grav (the app) has things hard coded to /var/www/html so I suggest you leave
#         the following alone unless you know what you are doing.

_version="1.3.2"

chmod a+w /dev/stderr
chmod a+w /dev/stdout

echo "NOTICE: entrypoint.sh v.${_version}"

echo "- starting rsyslogd daemon..."
rsyslogd

echo "- starting lighttpd..."
exec lighttpd -D -f /etc/lighttpd/lighttpd.conf

#echo "just keep container running..."
#tail -f /dev/null
